<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;
use Stripe\Stripe;
use Stripe\Charge;
use App\Models\Product;
use Darryldecode\Cart\Cart;
use App\Models\Order;


class CartController extends Controller
{
    public function shop()
    {
        $products = Product::all();
        //dd($products);
        return view('shop')->withTitle('E-COMMERCE STORE | SHOP')->with(['products' => $products]);
    }

    public function cart()  
    {
        $cartCollection = \Cart::getContent();
        //dd($cartCollection);
        return view('cart')->withTitle('E-COMMERCE STORE | CART')->with(['cartCollection' => $cartCollection]);;
    }

    public function add(Request$request){
        \Cart::add(array(
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'attributes' => array(
                'image' => $request->img,
                'slug' => $request->slug
            )
        ));
        return redirect()->route('cart.index')->with('success_msg', 'Item is Added to Cart!');

    }

     


    public function remove(Request $request){
        \Cart::remove($request->id);
        return redirect()->route('cart.index')->with('success_msg', 'Item is removed!');
    }

    public function update(Request $request){
        \Cart::update($request->id,
            array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->quantity
                ),
        ));
        return redirect()->route('cart.index')->with('success_msg', 'Cart is Updated!');
    }
    
    public function clear(){
        \Cart::clear();
        return redirect()->route('cart')->with('success_msg', 'Cart is cleared!');
    }

    // public function postCheckout(Request $request) {
    //    // \Cart::postCheckout($request->id);
    //    $cartCollection = \Cart::getContent($request->id);

    //     /* $stripe = new \Stripe\StripeClient(
    //         'sk_test_51IQoHyJ5y2Dh7xkg9zLM72NSKeaP6NiW1yRf9NlIEFmUdUYLSQKrTwwp9pVPXVa4XNK5SYDrciRqC1346pYWhsZX00leCNvFIF'
    //     ); */
    //     Stripe::setApiKey('sk_test_51IQoHyJ5y2Dh7xkg9zLM72NSKeaP6NiW1yRf9NlIEFmUdUYLSQKrTwwp9pVPXVa4XNK5SYDrciRqC1346pYWhsZX00leCNvFIF');
    //     try{
    //           /* $stripe->charges->create([ */
    //           Charge::create(array(
    //             'amount' => $cartCollection->getTotal *100, //  /Cart::getTotal(),
    //             'currency' => 'usd',
    //             'source' => $request->input('stripeToken'),
    //             'description' => 'Test Charge',
    //           ));
    //     }catch (\Exception $e){
    //         return redirect()->route('checkout')->with('error', $e->getMessage());
    //     }



    //     return redirect()->route('cart.index')->with('success_msg', 'Successfully purchased products!');
    // }

}

