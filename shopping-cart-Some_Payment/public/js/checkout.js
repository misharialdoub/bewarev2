Stripe.setPublishableKey('pk_test_51IQoHyJ5y2Dh7xkgbkC0i8Q960jtJwKCMVO2NVYCVBlWItaJe1HaLfmH6nrvzwAMKJ1btCFFaD6JFgCE0OzpzsJ200xP1b3Lox');

//have an event listener

var $form = $('#checkout-form');
    $('#charge-error').addClass('hidden');
    $form.find('button').prop('disabled', true);
$form.submit(function(event) {

    Stripe.card.createToken({
        number: $('#card-number').val(),
        cvc: $('#card-cvc').val(),
        exp_month: $('#card-expiry-month').val(),
        exp_year: $('#card-expiry-year').val(),
        name: $('#card-name').val()
    }, stripeResponseHandler);
    return false;
});

function stripeResponseHandler(status, response){
    if(response.error){
        $('#charge-error').removeClass('hidden');
        $('#charge-error').text(response.error.message);
        $form.find('button').prop('disabled', false);
    }else{
        var token = response.id;
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));

        //Submit the form:
        $form.get(0).submit();
    }
}



